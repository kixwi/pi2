#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

int main() {
  long double pi = 4.0; //Pi Final Value
  int deno = 3; //Denominator
  unsigned int hc_steps = 3000000; //hardcoded_steps
  int steps = 1;

  ofstream myfile ("pi.txt");
  myfile << setiosflags(ios::showpoint) << setiosflags(ios::fixed) << setprecision(100);
  cout << setiosflags(ios::showpoint) << setiosflags(ios::fixed) << setprecision(100);

  cout << "PI Calculator v1903" << endl; //v[year][week]

  while (steps <= hc_steps) {
    pi = pi - (4.0 / deno);
    myfile << pi << " : " << deno << "\n";
    deno = deno +2;
    cout << steps << " : " << pi << '\n';
    pi = pi + (4.0 /deno);
    myfile << pi << " : " << deno << "\n";
    deno = deno+2;
    cout << steps << " : " << pi << '\n';
    steps = steps+1;
    //cout << (steps/hc_steps)*100 << '\n';
    if (steps == hc_steps) {
      cout << "finish" << '\n';
    }
  }

  myfile << "final : " << '\n' << pi;
  cout << "file writed" << '\n';

  return 0;
}
